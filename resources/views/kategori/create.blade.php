@extends('layouts.base')

@section('title') Kategori Baru @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 inline-block">
        <h1 class="h3 mb-2 text-gray-800">Tambah Kategori</h1>
        <a href="/kategori" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm">Kembali</a>
    </div>

    <!-- DataTales Example -->
    <div class="col-md-5 ml-auto mr-auto">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Buat Kategori Baru</h6>
            </div>
            <form action="/kategori" method="POST" id="form">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <input name="nama" id="nama" type="text"
                            class="form-control @error('nama') is-invalid @enderror" value="{{ old('nama') }}"
                            placeholder="Nama Kategori">
                        @error('nama')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <textarea name="deskripsi_kategori" id="deskripsi_kategori" type="text"
                            class="form-control @error('deskripsi_kategori') is-invalid @enderror" rows="5"
                            placeholder="Deskripsi Kategori">{{ old('deskripsi_kategori') }}</textarea>
                        @error('deskripsi_kategori')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button class="btn btn-success" type="submit">Tambah</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection