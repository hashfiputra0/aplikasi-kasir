<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Profile;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profile =  [
            [
                'id' => '1',
                'no_hp' => '088888888888',
                'alamat' =>  'Jalan Admin Kasir',
                'jenis_kelamin' => 'Pria',
                'users_id' => '1',
                'akhir_ubah_oleh' => 'Seeder',
            ],
            [
                'id' => '2',
                'no_hp' => '081111111111',
                'alamat' =>  'Jalan Kasir',
                'jenis_kelamin' => 'Wanita',
                'users_is' => '2',
                'akhir_ubah_oleh' => 'Seeder',
            ]
        ];
        Profile::insert($profile);
    }
}
