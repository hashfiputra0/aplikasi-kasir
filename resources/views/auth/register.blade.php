@extends('layouts.app')

@section('title') Daftar @endsection
@section('content')
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-md-5 mt-5">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Daftar Akun</h1>
                        </div>
                        <form method="POST" action="{{ route('register') }}" class="user">
                            @csrf
                            <div class="form-group">
                                <input name="name" type="text" class="form-control form-control-user
                                @error('name') is-invalid @enderror" id="name" placeholder="Masukkan Nama Lengkap">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input name="email" type="email" class="form-control form-control-user
                                @error('email') is-invalid @enderror" id="email" placeholder="Masukkan Alamat Email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input name="password" type="password" class="form-control form-control-user
                                    @error('password') is-invalid @enderror" id="password"
                                        placeholder="Masukkan Kata Sandi">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control form-control-user" id="password-confirmation"
                                        placeholder="Konfirmasi Kata Sandi" name="password_confirmation">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                Daftar Masuk
                            </button>
                            <hr>
                        </form>
                        <hr>
                        <div class="text-center">
                            <a class="small" href="login">Sudah punya akun?</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
@endsection