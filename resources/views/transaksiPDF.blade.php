<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transaksi</title>
</head>

<body>
    <table class="table table-bordered">
        <thead>
            <tr class="text-center">
                <th>#</th>
                <th>Nama Barang</th>
                <th>Jumlah Barang</th>
                <th>Harga Satuan</th>
                <th>Total Harga</th>
                <th>Dibuat Pada</th>
                <th>Diubah Pada</th>
            </tr>
        </thead>
        <tbody width=100%>
            @forelse ($data as $key=>$value)
            <tr>
                <td class="text-center">{{$key + 1}}</th>
                <td>
                    @foreach ($value->barang as $barang)
                    {{$barang->nama_barang}},
                    @endforeach
                </td>
                <td>
                    @foreach ($value->transaksi_pembelian_barang as $jumlah)
                    {{$jumlah->jumlah}},
                    @endforeach
                </td>
                <td class="text-right">
                    @foreach ($value->barang as $harga_satuan)
                    Rp {{ number_format($harga_satuan->harga_satuan, 0, ',', '.') }},
                    @endforeach
                </td>
                <td class="text-right">Rp {{ number_format($value->total_harga, 0, ',', '.') }}</td>
                <td>{{$value->created_at}}</td>
                <td>{{$value->updated_at}}</td>
                <td>{{$value->akhir_ubah_oleh}}</td>
            </tr>
            @empty
            <tr>
                <td class="text-center" colspan="7">Tidak ada transaksi</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</body>

</html>