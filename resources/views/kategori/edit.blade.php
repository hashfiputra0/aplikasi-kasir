@extends('layouts.base')

@section('title') Ubah Kategori @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 inline-block">
        <h1 class="h3 mb-2 text-gray-800">Ubah Kategori</h1>
        <a href="/kategori" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm">Kembali</a>
    </div>

    <!-- DataTales Example -->
    <div class="col-md-5 ml-auto mr-auto">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Ubah Kategori {{ $kategori->id }}</h6>
            </div>
            <form action="/kategori/{{ $kategori->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <input name="nama" id="nama" type="text"
                            class="form-control @error('nama') is-invalid @enderror" value="{{ old('nama') ?? $kategori->nama }}"
                            placeholder="Nama Kategori">
                        @error('nama')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <textarea name="deskripsi_kategori" id="deskripsi_kategori" type="text"
                            class="form-control @error('deskripsi_kategori') is-invalid @enderror" rows="6"
                            placeholder="Deskripsi Kategori">{{ old('deskripsi_kategori') ?? $kategori->deskripsi_kategori }}</textarea>
                        @error('deskripsi_kategori')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button class="btn btn-success" type="submit">Ubah</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection