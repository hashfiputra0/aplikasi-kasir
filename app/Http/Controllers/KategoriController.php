<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Kategori;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::all();
        return view('kategori.index', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = kategori::all();
        return view('kategori.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|max:255',
            'deskripsi_kategori' => 'required|max:255'
        ],
        // Pesan error validate
        [
            'nama.required' => 'Nama kategori harus diisi.',
            'nama.max' => 'Maksimal panjang nama 255 karakter.',
            'deskripsi_kategori.required' => 'Deskripsi kategori harus diisi.',
            'deskripsi_kategori.max' => 'Maksimal panjang deskripsi 255 karakter.'
        ]);

        Kategori::create([
            'nama' => $request->nama,
            'deskripsi_kategori' => $request->deskripsi_kategori,
            'akhir_ubah_oleh' => Auth::user()->name
        ]);
        
        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/kategori')->with('status', 'Kategori Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = Kategori::find($id);
        return view('kategori.show', compact('kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::find($id);
        return view('kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|max:255',
            'deskripsi_kategori' => 'required|max:255'
        ],
        // Pesan error vaidate
        [
            'nama.required' => 'Nama kategori harus diisi.',
            'nama.max' => 'Maksimal nama kategori 255 karakter.',
            'deskripsi_kategori.required' => 'Deskripsi kategori harus diisi.',
            'deskripsi_kategori.max' => 'Maksimal deskripsi kategori 255 arakter.'
        ]);

        $kategori = Kategori::find($id);
        $kategori->nama = $request->nama;
        $kategori->deskripsi_kategori = $request->deskripsi_kategori;
        $kategori->akhir_ubah_oleh = Auth::user()->name;
        $kategori->update();

        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/kategori')->with('status', 'Kategori Berhasil Ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Kategori::find($id);
        $kategori->delete();

        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/kategori')->with('status', 'Kategori Berhasil Ditambahkan');
    }
}
