<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPembelianBarang extends Model
{
    use HasFactory;
    protected $table = 'transaksi_pembelian_barang';
    protected $guarded = [];
    public $timestamps = false;
    
    // Relasi
    public function transaksi_pembelian()
    {
        return $this->belongsToMany(TransaksiPembelian::class, 'transaksi_pembelian_id');
    }
    public function barang()
    {
        return $this->belongsToMany(MasterBarang::class, 'master_barang_id');
    }
}
