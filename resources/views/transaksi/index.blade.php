@extends('layouts.base')

@section('title') Transaksi @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between inline-block">
        <h1 class="h3 mb-0 text-gray-800 inline-block">Transaksi</h1>
        <div>
            @if (Auth::user()->role === "Admin Kasir")
            <a href="{{ URL::to('pdf-transaksi') }}"
                class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> PDF</a>
            @elseif (Auth::user()->role === "Kasir")
            @endif
            <a href="/transaksi/create" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Tambah
                Transaksi</a>
        </div>
    </div>
    <p class="mb-4 mt-0">Melihat, mengubah, menghapus transaksi yang ada.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Melihat Transaksi Yang Ada</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="datatables" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Kuantitas Barang</th>
                            <th>Harga Satuan</th>
                            <th>Total Harga</th>
                            <th>Dibuat Pada</th>
                            <th>Diubah Pada</th>
                            <th>Akhir Ubah Oleh</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="text-center">
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Kuantitas Barang</th>
                            <th>Harga Satuan</th>
                            <th>Total Harga</th>
                            <th>Dibuat Pada</th>
                            <th>Diubah Pada</th>
                            <th>Akhir Ubah Oleh</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @forelse ($transaksi as $key=>$value)
                        <tr>
                            <td class="text-center">{{$key + 1}}</td>
                            <td>
                                @foreach ($value->barang as $barang)
                                <span class="badge badge-pill badge-primary">{{$barang->nama_barang}}</span>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($value->transaksi_pembelian_barang as $jumlah)
                                <span class="badge badge-pill badge-danger">{{$jumlah->jumlah}}</span>
                                @endforeach
                            </td>
                            <td class="text-right">
                                @foreach ($value->barang as $harga_satuan)
                                <span class="badge badge-pill badge-secondary">Rp {{
                                    number_format($harga_satuan->harga_satuan, 0, ',', '.') }}</span>
                                @endforeach
                            </td>
                            <td class="text-right"><span class="badge badge-pill badge-success">Rp {{
                                    number_format($value->total_harga, 0, ',', '.') }}</span></td>
                            <td nowrap>{{$value->created_at}}</td>
                            <td nowrap>{{$value->updated_at}}</td>
                            <td nowrap>{{$value->akhir_ubah_oleh}}</td>
                            <td class="text-center" nowrap>
                                <a href="/transaksi/{{$value->id}}" class="btn btn-secondary">
                                    <i class="fas fa-search"></i>
                                </a>
                                <a href="pdf-transaksiOne/{{$value->id}}" class="btn btn-success">
                                    <i class="fas fa-download"></i>
                                </a>
                                <a href="#" data-toggle="modal" class="btn btn-danger"
                                    data-target="#delete{{$value->id}}">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                            <div class="modal fade" id="delete{{$value->id}}" tabindex="-1" role="dialog"
                                aria-labelledby="delete" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="delete">Hapus Transaksi</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">Anda yakin mau hapus transaksi
                                            {{ $value === 0 ? '' : $value->nama}}?</div>
                                        <div class="modal-footer">
                                            <button class="btn btn-success" type="button"
                                                data-dismiss="modal">Batal</button>
                                            <form action="/transaksi/{{$value->id}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="ti-layout-sidebar-left"></i> Hapus
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>

                        @empty
                        <tr>
                            <td class="text-center" colspan="9">Tidak ada transaksi</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js-datatables')
<!-- JS Datatables -->
<script src="{{ asset('sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('sb-admin-2/vendor/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('sb-admin-2/vendor/datatables/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function() {
			$('#datatables').DataTable({
                responsive: true,
                "pagingType": "full_numbers",
			});
        });
</script>
@endpush

@push('css-datatables')
<!-- CSS Datatables -->
<link href="{{ asset('sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('sb-admin-2/vendor/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet">
@endpush