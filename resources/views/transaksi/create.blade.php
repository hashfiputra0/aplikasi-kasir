@extends('layouts.base')

@section('title') Transaksi Baru @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-0 text-gray-800 inline-block">Tambah Transaksi</h1>
    <p class="small mb-4 mt-0">Pastikan barang yang sama tidak dipilih 2&times;</p>

    <!-- DataTales Example -->
    <div class="col-md-8 ml-auto mr-auto">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Buat Transaksi Baru</h6>
            </div>
            <form action="/transaksi" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-row" id="form-clone">
                        <div class="form-group col">
                            <select data-placeholder="Masukkan Barang" name="master_barang_id[]"
                                class="select2 form-control @error('master_barang_id') is-invalid @enderror"
                                data-allow-clear="1">
                                @foreach($barang as $value)
                                <option></option>
                                <option {{ old('master_barang_id[]')==$value->id ? 'selected' : '' }} value='{{
                                    $value->id }}'>
                                    {{ $value->nama_barang }} |
                                    Rp {{ number_format($value->harga_satuan, 0, ',', '.')}}
                                </option>
                                @endforeach
                            </select>
                            @error('master_barang_id[]')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-4">
                            <input name="jumlah[]" id="jumlah" type="text"
                                class="form-control @error('jumlah[]') is-invalid @enderror"
                                value="{{ old('jumlah[]') }}" placeholder="Kuantitas Barang">
                            @error('jumlah[]')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div id="wrapper-clone"></div>
                    <button class="btn btn-success" type="submit">Tambah</button>
                    <button class="btn btn-primary" type="button" id="add-clone">Tambah Barang</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('js-select2-bootstrap4')
<!-- Js Select2 Bootstrap 4 -->
<script src="{{ asset('select2-bootstrap4/dist/js/select2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.select2').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear'))
            });
        });
    });

    // Clone Form Barang
    $('#add-clone').on('click', function () {
        $myClone = $("#form-clone").first().clone();
        $myClone.find("span").remove();
        $myClone.find(".select2").select2({
            theme: 'bootstrap4',
            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            placeholder: $(this).data('placeholder'),
            allowClear: Boolean($(this).data('allow-clear'))
        });
        $("#wrapper-clone").append($myClone);
    });
</script>
@endpush

@push('css-select2-bootstrap4')
<!-- CSS Select2 Bootstrap 4 -->
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2-bootstrap4.min.css') }}">
@endpush