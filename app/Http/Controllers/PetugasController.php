<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\Profile;

class PetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $petugas = User::all();
        return view('petugas.index', compact('petugas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $petugas = Profile::all();
        return view('petugas.create', compact('petugas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'max:255'],
            'role' => ['required'],
            'no_hp' => ['required', 'max:15'],
            'alamat' => ['required', 'max:255'],
            'jenis_kelamin' => ['required']
        ],
        // Pesan error validate
        [
            'name.required' => 'Nama harus diisi.',
            'name.string' => 'Format nama salah.',
            'name.max' => 'Nama maksimal 255 karakter.',
            'email.required' => 'Email harus diisi.',
            'email.string' => 'Format email salah.',
            'email.email' => 'Format email salah.',
            'email.max' => 'Email maksimal 255 karakter.',
            'email.unique' => 'Email sudah terdaftar.',
            'password.required' => 'Password harus diisi.',
            'password.min' => 'Password minimal 8 karakter.',
            'password.max' => 'Password maksimal 255 karakter.',
            'role.required' => 'Role harus diisi.',
            'no_hp.required' => 'Nomor HP harus diisi.',
            'no_hp.max' => 'Maksimal nomor HP 15 karakter.',
            'alamat.required' => 'Alamat barang harus diisi.',
            'alamat.max' => 'Maksimal panjang alamat 255 karakter.',
            'jenis_kelamin.required' => 'Jenis kelamin harus diisi.'
        ]);
        
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'role' => $request['role']
        ]);

        Profile::create([
            'no_hp' => $request->no_hp,
            'alamat' => $request->alamat,
            'jenis_kelamin' => $request->jenis_kelamin,
            'users_id' => $user->id,
            'akhir_ubah_oleh' => Auth::user()->name
        ]);
        
        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/petugas')->with('status', 'Petugas Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $petugas = User::find($id);
        return view('petugas.show', compact('petugas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $petugas = User::find($id);
        return view('petugas.edit', compact('petugas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'max:255', 'min:8'],
            'role' => ['required'],
            'no_hp' => ['required', 'max:15'],
            'alamat' => ['required', 'max:255'],
            'jenis_kelamin' => ['required']
        ],
        // Pesan error validate
        [
            'name.required' => 'Nama harus diisi.',
            'name.string' => 'Format nama salah.',
            'name.max' => 'Nama maksimal 255 karakter.',
            'email.required' => 'Email harus diisi.',
            'email.string' => 'Format email salah.',
            'email.email' => 'Format email salah.',
            'email.max' => 'Email maksimal 255 karakter.',
            'email.unique' => 'Email sudah terdaftar.',
            'password.required' => 'Password harus diisi.',
            'password.max' => 'Password maksimal 255 karakter.',
            'password.min' => 'Password minimal 8 karakter.',
            'role.required' => 'Role harus diisi.',
            'no_hp.required' => 'Nomor HP harus diisi.',
            'no_hp.max' => 'Maksimal nomor HP 15 karakter.',
            'alamat.required' => 'Alamat barang harus diisi.',
            'alamat.max' => 'Maksimal panjang alamat 255 karakter.',
            'jenis_kelamin.required' => 'Jenis kelamin harus diisi.'
        ]);

        $petugas = User::find($id);
        $petugas->name = $request->name;
        $petugas->email = $request->email;
        $petugas->password = Hash::make($request->password);
        $petugas->role = $request->role;
        $petugas->profile->no_hp = $request->no_hp;
        $petugas->profile->alamat = $request->alamat;
        $petugas->profile->jenis_kelamin = $request->jenis_kelamin;
        $petugas->profile->akhir_ubah_oleh = Auth::user()->name;
        $petugas->update();
        $petugas->profile->update();

        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/petugas')->with('status', 'Petugas Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $petugas = User::find($id);
        $petugas->delete();

        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/petugas')->with('status', 'Petugas Berhasil Dihapus');
    }
}
