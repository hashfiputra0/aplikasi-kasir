@extends('layouts.base')

@section('title') Lihat Petugas @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 inline-block">
        <h1 class="h3 mb-2 text-gray-800">Lihat Petugas</h1>
        <a href="/petugas" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm">Kembali</a>
    </div>

    <!-- DataTales Example -->
    <div class="col-md-7 ml-auto mr-auto">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Lihat Petugas {{$petugas->id}}</h6>
            </div>
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td nowrap>
                                <span class="h5">Nama Petugas</span>
                            </td>
                            <td><p>{{$petugas->name}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Email</span>
                            </td>
                            <td><p>{{$petugas->email}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Role</span>
                            </td>
                            <td><p>{{$petugas->role}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Nomor HP</span>
                            </td>
                            <td><p>{{$petugas->profile->no_hp}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Alamat</span>
                            </td>
                            <td><p>{{$petugas->profile->alamat}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Jenis Kelamin</span>
                            </td>
                            <td><p>{{$petugas->profile->jenis_kelamin}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Dibuat Pada</span>
                            </td>
                            <td><p>{{$petugas->created_at}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Diubah Pada</span>
                            </td>
                            <td><p>{{$petugas->updated_at}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Akhir Ubah Oleh</span>
                            </td>
                            <td><p>{{$petugas->profile->akhir_ubah_oleh}}</p></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection