<?php

namespace Database\Seeders;

use App\Models\MasterBarang;
use Illuminate\Database\Seeder;

class MasterBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $barang =  [
            [
                'id' => '1',
                'nama_barang' => 'Sabun batang',
                'deskripsi_barang' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique porta est. Phasellus mattis, felis in aliquet rhoncus, odio justo molestie nulla, sit amet gravida enim neque nec tortor. Cras massa nisi, feugiat at purus sit amet, malesuada velit.',
                'harga_satuan' => '3000',
                'kategori_id' => '1',
                'akhir_ubah_oleh' => 'Seeder'
            ],
            [
                'id' => '2',
                'nama_barang' => 'Mi Instan',
                'deskripsi_barang' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique porta est. Phasellus mattis, felis in aliquet rhoncus, odio justo molestie nulla, sit amet gravida enim neque nec tortor. Cras massa nisi, feugiat at purus sit amet, malesuada velit.',
                'harga_satuan' => '2000',
                'kategori_id' => '2',
                'akhir_ubah_oleh' => 'Seeder'
            ],
            [
                'id' => '3',
                'nama_barang' => 'Pensil',
                'deskripsi_barang' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique porta est. Phasellus mattis, felis in aliquet rhoncus, odio justo molestie nulla, sit amet gravida enim neque nec tortor. Cras massa nisi, feugiat at purus sit amet, malesuada velit.',
                'harga_satuan' => '1000',
                'kategori_id' => '1',
                'akhir_ubah_oleh' => 'Seeder'
            ],
            [
                'id' => '4',
                'nama_barang' => 'Kopi sachet',
                'deskripsi_barang' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique porta est. Phasellus mattis, felis in aliquet rhoncus, odio justo molestie nulla, sit amet gravida enim neque nec tortor. Cras massa nisi, feugiat at purus sit amet, malesuada velit.',
                'harga_satuan' => '1500',
                'kategori_id' => '2',
                'akhir_ubah_oleh' => 'Seeder'
            ],
            [
                'id' => '5',
                'nama_barang' => 'Air minum galon',
                'deskripsi_barang' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique porta est. Phasellus mattis, felis in aliquet rhoncus, odio justo molestie nulla, sit amet gravida enim neque nec tortor. Cras massa nisi, feugiat at purus sit amet, malesuada velit.',
                'harga_satuan' => '20000',
                'kategori_id' => '1',
                'akhir_ubah_oleh' => 'Seeder'
            ]
        ];
        MasterBarang::insert($barang);
    }
}
