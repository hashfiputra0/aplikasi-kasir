<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transaksi {{$data->id}}</title>
</head>

<body>
    <h4>Data Transaksi Nomor {{$data->id}}</h4>
    <table class="table">
                    <tbody>
                        <tr nowrap>
                            <td>
                                Nama Barang
                            </td>
                            <td>
                                @foreach ($data->barang as $barang)
                                {{$barang->nama_barang}},
                                @endforeach
                            </td>
                        </tr>
                        <tr nowrap>
                            <td>
                                Kuantitas Barang
                            </td>
                            <td>
                                @foreach ($data->transaksi_pembelian_barang as $jumlah)
                                {{$jumlah->jumlah}},
                                @endforeach
                            </td>
                        </tr>
                        <tr nowrap>
                            <td>
                                Harga Satuan
                            </td>
                            <td>
                                @foreach ($data->barang as $harga_satuan)
                                Rp {{ number_format($harga_satuan->harga_satuan, 0, ',', '.') }},
                                @endforeach
                            </td>
                        </tr>
                        <tr nowrap>
                            <td>
                                Total Harga
                            </td>
                            <td>Rp {{ number_format($data->total_harga, 0, ',', '.') }}</td>
                        </tr>
                        <tr nowrap>
                            <td>
                                Dibuat Pada
                            </td>
                            <td>{{ $data->created_at }}</td>
                        </tr>
                        <tr nowrap>
                            <td>
                                Diubah Pada
                            </td>
                            <td>{{ $data->updated_at }}</td>
                        </tr>
                        <tr nowrap>
                            <td>
                                Akhir Ubah Oleh Pada
                            </td>
                            <td>{{ $data->akhir_ubah_oleh }}</td>
                        </tr>
                    </tbody>
                </table>
</body>

</html>