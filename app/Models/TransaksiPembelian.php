<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class TransaksiPembelian extends Model
{
    use HasFactory;
    protected $table = 'transaksi_pembelian';
    protected $guarded = [];

    // Ubah Format timestamp
    public function getCreatedAtAttribute()
    {
        Date::setLocale('id');
        return Date::parse($this->attributes['created_at'])
            ->format('l, d F Y H:i');
    }
    public function getUpdatedAtAttribute()
    {
        Date::setLocale('id');
        return Date::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    // Relasi
    public function transaksi_pembelian_barang()
    {
        return $this->hasMany(TransaksiPembelianBarang::class, 'transaksi_pembelian_id');
    }
    public function barang()
    {
        return $this->belongsToMany(MasterBarang::class, 'transaksi_pembelian_barang', 'transaksi_pembelian_id', 'master_barang_id');
    }
}
