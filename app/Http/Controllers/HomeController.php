<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use App\Models\User;
use App\Models\TransaksiPembelian;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $barang = MasterBarang::count();
        $petugas = User::count();
        $total_transaksi = TransaksiPembelian::count();
        return view('layouts.master', compact('barang', 'petugas', 'total_transaksi'));
    }
}
