<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterBarangTable extends Migration
{
    /**
     * Run the migrations.
     * @table master_barang
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_barang', function (Blueprint $table) {
            $table->id();
            $table->string('nama_barang');
            $table->string('deskripsi_barang');
            $table->bigInteger('harga_satuan');
            $table->foreignId('kategori_id')
                ->constrained('kategori')
                ->onUpdate('cascade')
                ->onDelete('no action');
            $table->string('akhir_ubah_oleh');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_barang');
    }
}
