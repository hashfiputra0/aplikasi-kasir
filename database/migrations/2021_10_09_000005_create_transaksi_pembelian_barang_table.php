<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiPembelianBarangTable extends Migration
{
    /**
     * Run the migrations.
     * @table transaksi_pembelian_barang
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_pembelian_barang', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transaksi_pembelian_id')
                ->constrained('transaksi_pembelian')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('master_barang_id')
                ->constrained('master_barang')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->bigInteger('jumlah');
            $table->bigInteger('harga_satuan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_pembelian_barang');
    }
}
