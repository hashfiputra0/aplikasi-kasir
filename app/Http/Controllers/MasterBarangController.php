<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\MasterBarang;
use App\Models\Kategori;

class MasterBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = MasterBarang::all();
        return view('barang.index', compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barang = MasterBarang::all();
        $kategori = Kategori::all();
        return view('barang.create', compact('barang', 'kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_barang' => 'required|max:255',
            'deskripsi_barang' => 'required|max:255',
            'harga_satuan' => 'required|numeric',
            'kategori_id' => 'required'
        ],
        // Pesan error validate
        [
            'nama_barang.required' => 'Nama barang harus diisi.',
            'nama_barang.max' => 'Maksimal panjang nama 255 karakter.',
            'deskripsi_barang.required' => 'Deskripsi barang harus diisi.',
            'deskripsi_barang.max' => 'Maksimal panjang deskripsi 255 karakter.',
            'harga_satuan.required' => 'Harga satuan barang harus diisi.',
            'harga_satuan.numeric' => 'Harga satuan harus berupa angka.',
            'kategori_id.required' => 'Kategori barang harus diisi.'
        ]);

        MasterBarang::create([
            'nama_barang' => $request->nama_barang,
            'deskripsi_barang' => $request->deskripsi_barang,
            'harga_satuan' => $request->harga_satuan,
            'kategori_id' => $request->kategori_id,
            'akhir_ubah_oleh' => Auth::user()->name
        ]);
        
        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/barang')->with('status', 'Barang Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = MasterBarang::find($id);
        return view('barang.show', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = MasterBarang::find($id);
        $kategori = Kategori::all();
        return view('barang.edit', compact('barang', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_barang' => 'required|max:255',
            'deskripsi_barang' => 'required|max:255',
            'harga_satuan' => 'required|numeric',
            'kategori_id' => 'required'
        ],
        // Pesan error validate
        [
            'nama_barang.required' => 'Nama barang harus diisi.',
            'nama_barang.max' => 'Maksimal panjang nama 255 karakter.',
            'deskripsi_barang.required' => 'Deskripsi barang harus diisi.',
            'deskripsi_barang.max' => 'Maksimal panjang deskripsi 255 karakter.',
            'harga_satuan.required' => 'Harga satuan barang harus diisi.',
            'harga_satuan.numeric' => 'Harga satuan harus berupa angka.',
            'kategori_id.required' => 'Kategori barang harus diisi.'
        ]);

        $barang = MasterBarang::find($id);
        $barang->nama_barang = $request->nama_barang;
        $barang->deskripsi_barang = $request->deskripsi_barang;
        $barang->harga_satuan = $request->harga_satuan;
        $barang->kategori_id = $request->kategori_id;
        $barang->akhir_ubah_oleh = Auth::user()->name;
        $barang->update();

        // SweetAlert2
        Session::flash('statusCode', 'success');
        return redirect('/barang')->with('status', 'Barang Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = MasterBarang::find($id);
        $barang->delete();

        // SweetAlert2
        Session::flash('statusCode', 'success');
        return redirect('/barang')->with('status', 'Barang Berhasil Dihapus');
    }
}
