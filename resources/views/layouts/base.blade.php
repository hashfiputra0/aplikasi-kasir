<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Projek JCC - Aplikasi Kasir">
	<meta name="author" content="Hashfi Putra">

	<title>KasirKu | @yield('title')</title>

	<!-- Custom fonts for this template-->
	<link href="{{ asset('sb-admin-2/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
	<link
		href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="{{ asset('sb-admin-2/css/sb-admin-2.min.css') }}" rel="stylesheet">

    @stack('css-datatables')

	@stack('css-select2-bootstrap4')

</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		@include('layouts.sidebar')
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				@include('layouts.navbar')
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				@yield('content')
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			@include('layouts.footer')
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="logout">Keluar</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Anda yakin mau keluar?</div>
				<div class="modal-footer">
					<button class="btn btn-success" type="button" data-dismiss="modal">Batal</button>
					<a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" class="btn btn-danger">
						<i class="ti-layout-sidebar-left"></i> Keluar
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="{{ asset('sb-admin-2/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('sb-admin-2/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

	<!-- Core plugin JavaScript-->
	<script src="{{ asset('sb-admin-2/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

	<!-- Custom scripts for all pages-->
	<script src="{{ asset('sb-admin-2/js/sb-admin-2.min.js') }}"></script>

	<!-- Page level plugins -->
	<script src="{{ asset('sb-admin-2/vendor/chart.js/Chart.min.js') }}"></script>

	<!-- Page level custom scripts -->
	<script src="{{ asset('js/demo/chart-area-demo.js') }}"></script>
	<script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>

	<!-- SweetAlert JS -->
    <script type="text/javascript" src="{{ asset('sweetalert2/js/sweetalert2.min.js') }}"></script>
    <script>
        @if (session('status'))
            swal({
                title: '{{ session('status')}}',
                icon: '{{ session('statusCode')}}',
                button: "Oke"
            });
        @endif
    </script>

    @stack('js-datatables')

	@stack('js-autonumeric')

	@stack('js-select2-bootstrap4')

</body>

</html>