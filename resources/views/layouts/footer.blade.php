<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; KasirKu 2021, By <a href="https://gitlab.com/hashfiputra0/">Hashfi Putra</a></span>
        </div>
    </div>
</footer>