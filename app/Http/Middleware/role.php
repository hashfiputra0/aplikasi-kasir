<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $currentRouteName=Route::currentRouteName();
        $userRole=auth()->user()->role;
        if (in_array ($currentRouteName, $this->userAccessRole()[$userRole])){
            return $next($request); 
        } else {
            abort (403,'Anda tidak diizinkan untuk mengakses halaman ini');
        }
    }
    private function userAccessRole()
    {
        return [
            'Admin Kasir'=>[
                'register',
                'index',
                'create',
                'home',
                'store',
                'logout',
                'barang.index',
                'barang.store',
                'barang.create',
                'barang.show',
                'barang.update',
                'barang.destroy',
                'barang.edit',
                'kategori.index',
                'kategori.store',
                'kategori.create',
                'kategori.show',
                'kategori.update',
                'kategori.destroy',
                'kategori.edit',
                'petugas.index',
                'petugas.store',
                'petugas.create',
                'petugas.show',
                'petugas.update',
                'petugas.destroy',
                'petugas.edit',
                'transaksi.index',
                'transaksi.store',
                'transaksi.create',
                'transaksi.show',
                'transaksi.update',
                'transaksi.destroy',
                'transaksi.edit',
                'pdf-transaksi',
                'pdf-transaksiOne'
            ],
            'Kasir'=>[
                'logout',
                'transaksi.index',
                'transaksi.store',
                'transaksi.create',
                'transaksi.show',
                'transaksi.update',
                'transaksi.destroy',
                'transaksi.edit',
            ]
        ];
    }
}
