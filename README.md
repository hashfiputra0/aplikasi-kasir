# Kelas JCC Partnership - Project Challenge - Aplikasi Kasir

## Screenshot
<p align="center"><img src="https://gitlab.com/hashfiputra0/aplikasi-kasir/-/raw/master/public/screenshot/1.png" width="450px" height="auto"> <img src="https://gitlab.com/hashfiputra0/aplikasi-kasir/-/raw/master/public/screenshot/2.png" width="450px" height="auto"></p>
<p align="center"><img src="https://gitlab.com/hashfiputra0/aplikasi-kasir/-/raw/master/public/screenshot/3.png" width="450px" height="auto"> <img src="https://gitlab.com/hashfiputra0/aplikasi-kasir/-/raw/master/public/screenshot/4.png" width="450px" height="auto"></p>

## ERD
![ERD](public/erd/erd.png)

## Library
1. [DataTables](datatables.net)
2. [SweetAlert](sweetalert.js.org)
3. [Select2](select2.org)

## Link
Link deploy aplikasi: https://aplikasi-kasirku.herokuapp.com/
* Akun Role Admin Kasir
  > Nama: Admin Kasir  
  > Email: adminkasir@adminkasir.com  
  > Password: 12345678
* Akun Role Kasir
  > Nama: Kasir  
  > Email: kasir@kasir.com  
  > Password: 12345678