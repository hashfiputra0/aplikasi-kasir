@extends('layouts.base')

@section('title') Barang Baru @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 inline-block">
        <h1 class="h3 mb-2 text-gray-800">Tambah Barang</h1>
        <a href="/barang" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm">Kembali</a>
    </div>

    <!-- DataTales Example -->
    <div class="col-md-5 ml-auto mr-auto">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Buat Barang Baru</h6>
            </div>
            <form action="/barang" method="POST" id="form">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <input name="nama_barang" id="nama_barang" type="text"
                            class="form-control @error('nama_barang') is-invalid @enderror"
                            value="{{ old('nama_barang') }}" placeholder="Nama Barang">
                        @error('nama_barang')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <textarea name="deskripsi_barang" id="deskripsi_barang" type="text"
                            class="form-control @error('deskripsi_barang') is-invalid @enderror" rows="6"
                            placeholder="Deskripsi Barang">{{ old('deskripsi_barang') }}</textarea>
                        @error('deskripsi_barang')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input name="harga_satuan" id="harga_satuan" type="tel"
                            class="form-control @error('harga_satuan') is-invalid @enderror"
                            value="{{ old('harga_satuan') }}" placeholder="Harga Satuan Barang">
                        @error('harga_satuan')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <select id="kategori_id" name="kategori_id"
                            class="form-control @error('kategori_id') is-invalid @enderror">
                            <option></option>
                            @foreach($kategori as $value)
                            <option {{ old('kategori_id') == $value->id ? 'selected' : '' }} value="{{ $value->id }}">
                                {{ $value->nama }}
                            </option>
                            @endforeach
                        </select>
                        @error('kategori_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button class="btn btn-success" type="submit">Tambah</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection

@push('js-autonumeric')
<!-- JS autoNumeric -->
<script src="{{ asset('autonumeric/dist/autoNumeric.min.js') }}"></script>
<script>
    new AutoNumeric('#harga_satuan',
        { 
            currencySymbol : 'Rp ',
            allowDecimalPadding: false,
            decimalCharacter: ",",
            decimalCharacterAlternative: ".",
            digitGroupSeparator: ".",
            unformatOnSubmit: true
        });
</script>
@endpush

@push('js-select2-bootstrap4')
<!-- Js Select2 Bootstrap 4 -->
<script src="{{ asset('select2-bootstrap4/dist/js/select2.min.js') }}"></script>
<script>
    $(function () {
        $('select').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                placeholder: "Kategori Barang",
                allowClear: true
            });
        });
    });
</script>
@endpush

@push('css-select2-bootstrap4')
<!-- CSS Select2 Bootstrap 4 -->
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2-bootstrap4.min.css') }}">
@endpush