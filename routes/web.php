<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\MasterBarangController;
use App\Http\Controllers\PetugasController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\PDFController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum', 'verified','role'])->group(function () {
    Route::resources([
        '/' => HomeController::class,
        'kategori' => KategoriController::class,
        'barang' => MasterBarangController::class,
        'petugas' => PetugasController::class
    ]);
    Route::resource('transaksi', TransaksiController::class)->except([
        'update', 'edit'
    ]);
    Route::get('pdf-transaksi', [PDFController::class, 'generatePDFTransaksi'])->name('pdf-transaksi');
    Route::get('/pdf-transaksiOne/{transaksi_pembelian_id}', [PDFController::class, 'generatePDFTransaksiOne'])->name('pdf-transaksiOne');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
