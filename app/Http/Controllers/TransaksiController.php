<?php

namespace App\Http\Controllers;

use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;
use App\Models\MasterBarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = TransaksiPembelian::all();
        return view('transaksi.index', compact('transaksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transaksi = TransaksiPembelianBarang::all();
        $barang = MasterBarang::all();
        return view('transaksi.create', compact('transaksi', 'barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'master_barang_id.*' => ['required'],
            'jumlah.*' => ['required', 'numeric', 'gt:0']
        ]);

        $master_barang_id = array_map('intval', $request->master_barang_id);
        $jumlah = array_map('intval', $request->jumlah);
        $harga_satuan = MasterBarang::whereIn('id', $master_barang_id)->get('harga_satuan');

        foreach ($master_barang_id as $key => $no) {
            $input['harga_satuan'] = $harga_satuan[$key]['harga_satuan'];
            $input['jumlah'] = $jumlah[$key];
            $subtotal[] = $input['harga_satuan'] * $input['jumlah'];
        }

        $total_harga = array_sum($subtotal);

        $transaksi = TransaksiPembelian::create([
            'total_harga' => $total_harga,
            'akhir_ubah_oleh' => Auth::user()->name
        ]);

        foreach ($master_barang_id as $key => $no) {
            $input['transaksi_pembelian_id'] = $transaksi->id;
            $input['master_barang_id'] = $no;
            $input['jumlah'] = $jumlah[$key];
            $input['harga_satuan'] = $harga_satuan[$key]['harga_satuan'];
            TransaksiPembelianBarang::create($input);
        }
        
        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/transaksi')->with('status', 'Transaksi Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksi = TransaksiPembelian::find($id);
        return view('transaksi.show', compact('transaksi'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = TransaksiPembelian::find($id);
        $transaksi->delete();

        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/transaksi')->with('status', 'Transaksi Berhasil Ditambahkan');
    }
}
