<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MasterBarang extends Model
{
    use HasFactory;
    protected $table = 'master_barang';
    protected $guarded = [];

    // Ubah Format timestamp
    public function getCreatedAtAttribute()
    {
        Date::setLocale('id');
        return Date::parse($this->attributes['created_at'])
            ->format('l, d F Y H:i');
    }
    public function getUpdatedAtAttribute()
    {
        Date::setLocale('id');
        return Date::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }

    // Relasi
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }
    public function transaksi_pembelian_barang()
    {
        return $this->hasMany(TransaksiPembelianBarang::class, 'master_barang_id');
    }
}
