@extends('layouts.base')

@section('title') Lihat Petugas @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 inline-block">
        <h1 class="h3 mb-2 text-gray-800">Lihat Transaksi</h1>
        <div>
            @if (Auth::user()->role === "Admin Kasir")
            <a href="/pdf-transaksiOne/{{$transaksi->id}}"
                class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> PDF</a>
            @elseif (Auth::user()->role === "Kasir")
            @endif
            <a href="/transaksi" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm">Kembali</a>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="col-md-7 ml-auto mr-auto">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Lihat Transaksi {{$transaksi->id}}</h6>
            </div>
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr nowrap>
                            <td>
                                <span class="h5">Nama Barang</span>
                            </td>
                            <td class="text-right">
                                @foreach ($transaksi->barang as $barang)
                                <span class="badge badge-pill badge-primary">{{$barang->nama_barang}}</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr nowrap>
                            <td>
                                <span class="h5">Kuantitas Barang</span>
                            </td>
                            <td class="text-right">
                                @foreach ($transaksi->transaksi_pembelian_barang as $jumlah)
                                <span class="badge badge-pill badge-danger">{{$jumlah->jumlah}}</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr nowrap>
                            <td>
                                <span class="h5">Harga Satuan</span>
                            </td>
                            <td class="text-right">
                                @foreach ($transaksi->barang as $harga_satuan)
                                <span class="badge badge-pill badge-secondary">Rp {{
                                    number_format($harga_satuan->harga_satuan, 0, ',', '.') }}</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr nowrap>
                            <td>
                                <span class="h5">Total Harga</span>
                            </td>
                            <td class="text-right">
                                <span class="badge badge-pill badge-success">
                                    Rp {{ number_format($transaksi->total_harga, 0, ',', '.') }}
                                </span>
                            </td>
                        </tr>
                        <tr nowrap>
                            <td>
                                <span class="h5">Dibuat Pada</span>
                            </td>
                            <td class="text-right">{{ $transaksi->created_at }}</td>
                        </tr>
                        <tr nowrap>
                            <td>
                                <span class="h5">Diubah Pada</span>
                            </td>
                            <td class="text-right">{{ $transaksi->updated_at }}</td>
                        </tr>
                        <tr nowrap>
                            <td>
                                <span class="h5">Akhir Ubah Oleh Pada</span>
                            </td>
                            <td class="text-right">{{ $transaksi->akhir_ubah_oleh }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection