@extends('layouts.base')

@section('title') Lihat Barang @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 inline-block">
        <h1 class="h3 mb-2 text-gray-800">Lihat Barang</h1>
        <a href="/barang" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm">Kembali</a>
    </div>

    <!-- DataTales Example -->
    <div class="col-md-7 ml-auto mr-auto">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Lihat Barang {{$barang->id}}</h6>
            </div>
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td nowrap>
                                <span class="h5">Nama</span>
                            </td>
                            <td><p>{{$barang->nama_barang}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Deskripsi</span>
                            </td>
                            <td><p>{{$barang->deskripsi_barang}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Harga</span>
                            </td>
                            <td><p>Rp {{ number_format($barang->harga_satuan, 2, ',', '.')}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Kategori</span>
                            </td>
                            <td><p>{{$barang->kategori->nama}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Dibuat Pada</span>
                            </td>
                            <td><p>{{$barang->created_at}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Diubah Pada</span>
                            </td>
                            <td><p>{{$barang->updated_at}}</p></td>
                        </tr>
                        <tr>
                            <td nowrap>
                                <span class="h5">Akhir Ubah Oleh</span>
                            </td>
                            <td><p>{{$barang->akhir_ubah_oleh}}</p></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection