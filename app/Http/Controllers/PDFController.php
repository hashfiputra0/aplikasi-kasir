<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TransaksiPembelian;
use App\Models\User;
use App\Models\MasterBarang;
use PDF;

class PDFController extends Controller
{
    public function generatePDFTransaksi()
    {
        $data = TransaksiPembelian::all();
        $pdf = PDF::loadView('transaksiPDF', compact('data'));
        return $pdf->download('transaksi.pdf');
    }
    public function generatePDFTransaksiOne($id)
    {
        $data = TransaksiPembelian::find($id);
        $pdf = PDF::loadView('transaksiPDFOne', compact('data'));
        return $pdf->download('transaksi.pdf');
    }
}
