@extends('layouts.base')

@section('title') Petugas Baru @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 inline-block">
        <h1 class="h3 mb-2 text-gray-800">Tambah Petugas</h1>
        <a href="/petugas" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm">Kembali</a>
    </div>

    <!-- DataTales Example -->
    <div class="col-md-5 ml-auto mr-auto">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Buat Petugas Baru</h6>
            </div>
            <form action="/petugas" method="POST" id="form">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <input name="name" id="name" type="text"
                            class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}"
                            placeholder="Nama Lengkap" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input name="email" id="email" type="text"
                            class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                            placeholder="Email Petugas">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input name="password" id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" placeholder="Password Petugas">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <select id="role" name="role" class="form-control @error('role') is-invalid @enderror">
                            <option></option>
                            <option {{ old('role') == "Kasir" ? 'selected' : '' }} value="Kasir">
                                Kasir
                            </option>
                            <option {{ old('role') == "Admin Kasir" ? 'selected' : '' }} value="Admin Kasir">
                                Admin Kasir
                            </option>
                        </select>
                        @error('role')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input name="no_hp" id="no_hp" type="tel"
                            class="form-control @error('no_hp') is-invalid @enderror" value="{{ old('no_hp') }}"
                            placeholder="Nomor Telepon">
                        @error('no_hp')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <textarea name="alamat" id="alamat" class="form-control @error('alamat') is-invalid @enderror"
                            placeholder="Alamat Petugas">{{ old('alamat') }}</textarea>
                        @error('alamat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <select id="jenis_kelamin" name="jenis_kelamin"
                            class="form-control @error('jenis_kelamin') is-invalid @enderror">
                            <option></option>
                            <option {{ old('jenis_kelamin') ? 'selected' : '' }} value="Pria">
                                Pria
                            </option>
                            <option {{ old('jenis_kelamin') ? 'selected' : '' }} value="Wanita">
                                Wanita
                            </option>
                        </select>
                        @error('jenis_kelamin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button class="btn btn-success" type="submit">Tambah</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection

@push('js-autonumeric')
<!-- JS autoNumeric -->
<script src="{{ asset('autonumeric/dist/autoNumeric.min.js') }}"></script>
<script>
    new AutoNumeric('#harga_satuan',
        { 
            currencySymbol : 'Rp ',
            allowDecimalPadding: false,
            decimalCharacter: ",",
            decimalCharacterAlternative: ".",
            digitGroupSeparator: ".",
            unformatOnSubmit: true
        });
</script>
@endpush

@push('js-select2-bootstrap4')
<!-- Js Select2 Bootstrap 4 -->
<script src="{{ asset('select2-bootstrap4/dist/js/select2.min.js') }}"></script>
<script>
    $(function () {
        $('#jenis_kelamin').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                placeholder: "Jenis Kelamin",
                allowClear: true,
                minimumResultsForSearch: -1
            });
        });
        $('#role').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                placeholder: "Role Petugas",
                allowClear: true,
                minimumResultsForSearch: -1
            });
        });
    });
</script>
@endpush

@push('css-select2-bootstrap4')
<!-- CSS Select2 Bootstrap 4 -->
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2-bootstrap4.min.css') }}">
@endpush