@extends('layouts.base')

@section('title') Petugas @endsection
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between inline-block">
        <h1 class="h3 mb-2 text-gray-800">Petugas</h1>
        <a href="/petugas/create" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Tambah
            Petugas</a>
    </div>
    <p class="mb-4">Melihat, mengubah, menghapus barang yang ada.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Melihat Petugas Yang Ada</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover" id="datatables" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>No. HP</th>
                            <th>Alamat</th>
                            <th>Jenis Kelamin</th>
                            <th>Dibuat Pada</th>
                            <th>Diubah Pada</th>
                            <th>Akhir Ubah Oleh</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="text-center">
                            <th>#</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>No. HP</th>
                            <th>Alamat</th>
                            <th>Jenis Kelamin</th>
                            <th>Dibuat Pada</th>
                            <th>Diubah Pada</th>
                            <th>Akhir Ubah Oleh</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @forelse ($petugas as $key=>$value)
                        <tr>
                            <td class="text-center">{{$key + 1}}</th>
                            <td>{{$value->name}}</td>
                            <td>{{$value->email}}</td>
                            <td>{{$value->role}}</td>
                            <td>{{$value->profile->no_hp}}</td>
                            <td>{{$value->profile->alamat}}</td>
                            <td>{{$value->profile->jenis_kelamin}}</td>
                            <td>{{$value->profile->created_at}}</td>
                            <td>{{$value->profile->updated_at}}</td>
                            <td>{{$value->profile->akhir_ubah_oleh}}</td>
                            <td class="text-center">
                                <a href="/petugas/{{$value->id}}" class="btn btn-secondary">
                                    <i class="fas fa-search"></i>
                                </a>
                                <a href="/petugas/{{$value->id}}/edit" class="btn btn-primary">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="#" data-toggle="modal"  class="btn btn-danger" data-target="#delete{{$value->id}}">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                            <div class="modal fade" id="delete{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="delete"
                                aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="delete">Hapus Petugas</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">Anda yakin mau hapus petugas
                                            {{ $value === 0 ? '' : $value->nama}}?</div>
                                        <div class="modal-footer">
                                            <button class="btn btn-success" type="button"
                                                data-dismiss="modal">Batal</button>
                                            <form action="/petugas/{{$value->id}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="ti-layout-sidebar-left"></i> Hapus
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>

                        @empty
                        <tr>
                            <td class="text-center" colspan="11">Tidak ada petugas</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js-datatables')
<!-- JS Datatables -->
<script src="{{ asset('sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('sb-admin-2/vendor/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('sb-admin-2/vendor/datatables/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function() {
			$('#datatables').DataTable({
                responsive: true,
                "pagingType": "full_numbers",
			});
        });
</script>
@endpush

@push('css-datatables')
<!-- CSS Datatables -->
<link href="{{ asset('sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link href="{{ asset('sb-admin-2/vendor/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet">
@endpush