<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-file-invoice-dollar"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Kasir <sup>Ku</sup></div>
    </a>

    @if (Auth::user()->role === "Admin Kasir")
    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="/">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dasbor Admin Kasir</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Menu Admin Kasir
    </div>

    <!-- Nav Item - Barang -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#barang" aria-expanded="true"
            aria-controls="barang">
            <i class="fas fa-newspaper"></i>
            <span>Barang</span>
        </a>
        <div id="barang" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="/barang">Barang Yang Ada</a>
                <a class="collapse-item" href="/barang/create">Tambah Barang</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Kategori -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#kategori" aria-expanded="true"
            aria-controls="kategori">
            <i class="fas fa-list"></i>
            <span>Kategori</span>
        </a>
        <div id="kategori" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="/kategori">Kategori Yang Ada</a>
                <a class="collapse-item" href="/kategori/create">Tambah Kategori</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Petugas -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#petugas" aria-expanded="true"
            aria-controls="petugas">
            <i class="fas fa-users"></i>
            <span>Petugas</span>
        </a>
        <div id="petugas" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="/petugas">Petugas Yang Ada</a>
                <a class="collapse-item" href="/petugas/create">Tambah Petugas</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Transaksi -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#transaksi" aria-expanded="true"
            aria-controls="transaksi">
            <i class="fas fa-shopping-cart"></i>
            <span>Transaksi</span>
        </a>
        <div id="transaksi" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="/transaksi">Transaksi Yang Ada</a>
                <a class="collapse-item" href="/transaksi/create">Tambah Transaksi</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    @elseif(Auth::user()->role === "Kasir")
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Menu Kasir
    </div>

    <!-- Nav Item - Transaksi -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#transaksi" aria-expanded="true"
            aria-controls="transaksi">
            <i class="fas fa-shopping-cart"></i>
            <span>Transaksi</span>
        </a>
        <div id="transaksi" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="/transaksi">Transaksi Yang Ada</a>
                <a class="collapse-item" href="/transaksi/create">Tambah Transaksi</a>
            </div>
        </div>
    </li>
    @endif

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>