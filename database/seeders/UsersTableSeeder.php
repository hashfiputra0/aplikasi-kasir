<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user =  [
            [
                'id' => '1',
                'name' => 'Admin Kasir',
                'email' => 'adminkasir@adminkasir.com',
                'password' =>  Hash::make('12345678'),
                'role' => 'Admin Kasir',
            ],
            [
                'id' => '2',
                'nama_barang' => 'Kasir',
                'email' => 'kasir@kasir.com',
                'password' =>  Hash::make('12345678'),
                'role' => 'Kasir',
            ]
        ];
        User::insert($user);
    }
}
