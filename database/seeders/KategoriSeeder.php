<?php

namespace Database\Seeders;

use App\Models\Kategori;
use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori =  [
            [
                'id' => '1',
                'nama' => 'Barang Kebutuhan',
                'deskripsi_kategori' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique porta est. Phasellus mattis, felis in aliquet rhoncus, odio justo molestie nulla, sit amet gravida enim neque nec tortor. Cras massa nisi, feugiat at purus sit amet, malesuada velit.',
                'akhir_ubah_oleh' => 'Seeder'
            ],
            [
                'id' => '2',
                'nama_barang' => 'Makanan dan Minuman',
                'deskripsi_kategori' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique porta est. Phasellus mattis, felis in aliquet rhoncus, odio justo molestie nulla, sit amet gravida enim neque nec tortor. Cras massa nisi, feugiat at purus sit amet, malesuada velit.',
                'akhir_ubah_oleh' => 'Seeder'
            ]
        ];
        Kategori::insert($kategori);
    }
}
